all : src/Lanceur.java
	javac -d bin/ src/*.java
	cp -R res/ bin/
	echo "Main-Class: patatedouce.algo.Lanceur" > bin/MANIFEST.mf
	cd bin; jar -cvmf MANIFEST.mf Algo.jar .; mv Algo.jar ..
clean :
	rm *~
