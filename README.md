Java implementation of some algorithms based on the book "Introduction to Algorithms" by Cormen, Leiserson, Rivest and Stein

<br>

Already done :
1. Sort
    * Insertion sort
    * Selection sort
    * Merge sort
    * Bubble sort
    * Heapsort

<br>

To execute go to the **bin/** directory, then type ´java -jar Sorting.jar´