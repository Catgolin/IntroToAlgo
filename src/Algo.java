package patatedouce.algo;

abstract class Algo extends Thread {
	public enum Type {
		BULLE, SELECTION
	}
	private int etape = 0;
	public long fps = 200;
	private long dureeFrame;
	public boolean tourne = false;
	public abstract String nom();
	public int etape() {
		return this.etape;
	}
	public abstract int actif();
	public abstract boolean estTrie(int i);
	public void run() {
		long debutFrame = System.currentTimeMillis();
		while(true) {
			this.dureeFrame = 1000 / this.fps;
			if(System.currentTimeMillis() - debutFrame < this.dureeFrame) {
				try {
					Thread.sleep(this.dureeFrame - System.currentTimeMillis() + debutFrame);
				} catch(InterruptedException e) {
					System.err.print("Thread de l'algorithme interrompu");
				}
			}
			if(this.tourne) {
				this.stepUp();
			}
			debutFrame = System.currentTimeMillis();
		}
	}
	public void stepUp() {
		this.runEtape();
		this.etape++;
	}
	protected abstract void runEtape();
}

class TriBulle extends Algo {
	private Univers liste;
	private int i = 0;
	private int j = 0;
	public TriBulle(Univers univers) {
		this.liste = univers;
	}
	public String nom() {
		return "Tri à bulles";
	}
	public int actif() {return this.i;}
	public boolean estTrie(int i) {
		return i >= (this.liste.size() - this.j);
	}
	public void runEtape() {
		if(this.j < this.liste.size() && this.j > -1) {
			if(this.i < this.liste.size() - this.j - 1 && this.i > -1) {
				if(this.liste.get(i).getValeur() < this.liste.get(i+1).getValeur()) {
					this.liste.swap(i, i+1);
				}
				this.i++;
			} else {
				this.j ++;
				this.i = 0;
			}
		} else {
			this.stop();
		}
	}
}

class TriSelection extends Algo {
	private Univers liste;
	private int i = 0;
	private int j = 0;
	private int min = 0;
	public TriSelection(Univers univers) {
		this.liste = univers;
	}
	public String nom() {return "Tri par sélection";}
	public int actif() {return this.i;}
	public boolean estTrie(int i) {return i < this.j;}
	public void runEtape() {
		if(this.j < this.liste.size() && this.j > -1) {
			if(this.i < this.liste.size() && this.i > -1) {
				if(this.liste.get(this.i).getValeur() > this.liste.get(this.min).getValeur()) {
					this.min = this.i;
				}
				this.i++;
			} else {
				if(this.liste.get(this.j).getValeur() < this.liste.get(this.min).getValeur()) {
					this.liste.swap(this.j, this.min);
				}
				this.j ++;
				this.i = this.j;
				this.min = this.i;
			}
		} else {
			this.stop();
		}
	}
}
