package patatedouce.algo;

import java.util.ArrayList;

public class Univers extends ArrayList<Donnee> {
	public Univers() {
		super();
	}
	public Univers(int taille) {
		this();
		for(int i = 0; i < taille; i++) {
			this.add(new Donnee());
		}
	}
	public Univers(int taille, int amplitude) {
		this();
		for(int i = 0; i < taille; i++) {
			this.add(new Donnee((int)(Math.random() * amplitude)));
		}
	}
	public Donnee max() {
		int valeurMax = Integer.MIN_VALUE;
		Donnee max = null;
		for(int i = 0; i < this.size(); i++) {
			if(this.get(i).getValeur() > valeurMax) {
				max = this.get(i);
				valeurMax = max.getValeur();
			}
		}
		return max;
	}
	public Donnee min() {
		int valeurMin = Integer.MAX_VALUE;
		Donnee min = null;
		for(int i = 0; i < this.size(); i++) {
			if(this.get(i).getValeur() < valeurMin) {
				min = this.get(i);
				valeurMin = min.getValeur();
			}
		}
		return min;
	}
	public void swap(int i, int j) {
		Donnee tmp = this.get(i);
		this.set(i, this.get(j));
		this.set(j, tmp);
	}
}
