package sorting;

public class MaxHeap{
    private int heapsize;

    public MaxHeap(int[] a){
	heapsize = a.length;
	for(int i=heapsize/2; i>=1; i--){
	    max_heapify(a, i);
	}
    }

    public void decrement_heapsize(){
	heapsize --;
    }
    
    public int parent(int i){
	return i >> 1;
    }
    public int left(int i){
	return i << 1;
    }
    public int right(int i){
	return (i << 1) + 1;
    }

    public void max_heapify(int[] a, int i){
	int l = left(i);
	int r = right(i);
	int largest;
	if(l <= heapsize && a[l-1] > a[i-1]){
	    largest = l;
	}else{
	    largest = i;
	}
	if(r <= heapsize && a[r-1] > a[largest-1]){
	    largest = r;
	}
	if(largest != i){
	    ArrayUtilities.swap(a, i-1, largest-1);
	    max_heapify(a, largest);
	}
    }
}
