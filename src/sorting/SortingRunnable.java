package sorting;

import view.SortProcessView;

public class SortingRunnable implements Runnable{
    private Sort s;
    private int[] arrayToSort;
    private int[] sortedArray;
    private SortProcessView spv;
    
    public SortingRunnable(Sort s, int[] arrayToSort, SortProcessView spv){
	this.s = s;
	this.arrayToSort = arrayToSort;
	this.spv = spv;
    }

    @Override
    public void run(){
	long begin = System.currentTimeMillis();
	sortedArray = s.sort(arrayToSort, () -> {
		spv.repaint();
		try{
		    Thread.sleep(1);
		}catch(Exception e){}
	    });
	System.out.println("["+s.toString()+"]");
	System.out.println("Size : "+arrayToSort.length);
	System.out.println("Time : "+(System.currentTimeMillis() - begin)+" ms\n");
    }
}
