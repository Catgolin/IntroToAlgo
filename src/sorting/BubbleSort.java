package sorting;

public class BubbleSort implements Sort{
    @Override
    public int[] sort(int[] a, StepAction sa){
	for(int i=0; i<a.length-1; i++){
	    for(int j=a.length-1; j>i; j--){
		if(a[j] < a[j-1]){
		    ArrayUtilities.swap(a, j, j-1);
		}
	    }
	    sa.doAction();
	}
	return a;
    }

    @Override
    public String toString(){
	return "Bubblesort";
    }
}
