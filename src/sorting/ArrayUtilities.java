package sorting;

class ArrayUtilities{
    public static void swap(int[] a, int ind1, int ind2){
	int tmp = a[ind1];
	a[ind1] = a[ind2];
	a[ind2] = tmp;
    }
	
    
    public static int min(int[] a, int p, int q){
	int min = a[p];
	int ind = p;
	for(int i=p+1; i<q; i++){
	    if(min > a[i]){
		min = a[i];
		ind = i;
	    }
	}
	return ind;
    }

    public static void showArray(int[] a){
	for(int i=0; i<a.length; i++){
	    System.out.print(a[i]+" ");
	}
	System.out.println();
    }
}
