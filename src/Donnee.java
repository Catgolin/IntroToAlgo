package patatedouce.algo;

public class Donnee implements Comparable<Donnee> {
	int valeur;
	public Donnee() {
		this.valeur = (int)(Math.random() * 100);
	}
	public Donnee(int valeur) {
		this.valeur = valeur;
	}
	public void setValeur(int i) {
		this.valeur = i;
	}
	public int getValeur() {
		return this.valeur;
	}
	@Override public int compareTo(Donnee d) {
		return d.getValeur() - this.getValeur();
	}
}
