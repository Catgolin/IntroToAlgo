package patatedouce.algo;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Vue extends JFrame implements Runnable, MouseListener, KeyListener {
	private long dureeFrame = 1000/30;
	private Univers donnees;
	private Algo algo;
	private Algo.Type typeAlgo = Algo.Type.BULLE;
	private int tailleTableau = 100;
	private int amplitude = 1000;
	private boolean ctrl = false;
	public Vue() {
		this.donnees = new Univers();
		this.setTitle("Exemple de tri");
		this.setSize(1000, 800);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(new Vue.Panneau());
		this.addMouseListener(this);
		this.addKeyListener(this);
		this.setVisible(true);
	}
	@Override public void run() {
		long lastFrame = System.currentTimeMillis();
		while(true) {
			if(System.currentTimeMillis() - lastFrame < this.dureeFrame) {
				try {
					Thread.sleep(this.dureeFrame - System.currentTimeMillis() + lastFrame);
				} catch(InterruptedException e) {
					System.err.print("Interruption du processus graphique");
				}
			}
			this.repaint();
			lastFrame = System.currentTimeMillis();
		}
	}
	public void setDonnees(Univers donnees) {
		this.donnees = donnees;
	}
	public void setAlgo(Algo algo) {
		this.algo = algo;
	}
	public void peindre(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		float largeur = (float)this.getWidth() / (float)this.donnees.size();
		float hauteur = (float)(this.getHeight() - 200) / (float)this.donnees.max().getValeur();
		for(int i = 0; i < this.donnees.size(); i++) {
			Donnee d = this.donnees.get(i);
			if(this.algo.actif() == i) {
				g.setColor(Color.RED);
			} else if(this.algo.estTrie(i)) {
				g.setColor(Color.GREEN);
			} else {
				g.setColor(Color.WHITE);
			}
			int x =(int)((float)(i) * largeur);
			int y = (int)(hauteur * d.getValeur());
			g.fillRect(x, y, (int)largeur, this.getHeight() - 200);
			g.setColor(Color.BLACK);
			g.drawRect(x, y, (int)largeur, this.getHeight() - 200);
		}
		g.setColor(Color.GRAY);
		g.fillRect(0, this.getHeight() - 200, this.getWidth(), 200);
		g.setColor(Color.BLACK);
		g.drawString("Algorithme : "+this.algo.nom(), 30, this.getHeight() - 150);
		g.drawLine(30, this.getHeight() - 125, this.getWidth() - 30, this.getHeight() - 125);
		g.drawString("Étape : "+this.algo.etape(), 30, this.getHeight() - 100);
		g.drawRect(195, this.getHeight()-115, 20, 20);
		g.drawString("+", 200, this.getHeight() - 100);
		g.drawLine(30, this.getHeight() - 90, this.getWidth() - 30, this.getHeight() - 90);
		g.drawRect(25, this.getHeight()-65, 100, 20);
		g.drawString("Boucler >>", 30, this.getHeight() - 50);
		g.drawLine(250, this.getHeight() - 170, 250, this.getHeight() - 30);
		g.drawString("Taille du tableau : "+this.donnees.size(), 270, this.getHeight() - 150);
		g.drawString("Fps :", 270, this.getHeight() - 100);
		g.drawString("-", 310, this.getHeight() - 100);
		g.drawString(""+this.algo.fps, 330, this.getHeight() - 100);
		g.drawString("+", 360, this.getHeight() - 100);
		g.drawRect(305, this.getHeight() - 115, 20, 20);
		g.drawRect(355, this.getHeight() - 115, 20, 20);
		g.drawString("Retour >>", 280, this.getHeight() - 50);
		g.drawRect(270, this.getHeight()-65, 100, 20);
	}
	public void afficherMenu(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.WHITE);
		g.drawString("Algorithme :", this.getWidth() / 4, this.getHeight() / 5);
		if(this.typeAlgo == Algo.Type.BULLE) {
			g.fillRect(this.getWidth() / 4 + 95, this.getHeight() / 5 - 15, 85, 20);
			g.setColor(Color.BLACK);
		} else {
			g.setColor(Color.WHITE);
		}
		g.drawString("Bulles", this.getWidth() / 4 + 100, this.getHeight() / 5);
		g.drawRect(this.getWidth() / 4 + 95, this.getHeight() / 5 - 15, 85, 20);
		if(this.typeAlgo == Algo.Type.SELECTION) {
			g.fillRect(this.getWidth() / 4 + 195, this.getHeight() / 5 - 15, 85, 20);
			g.setColor(Color.BLACK);
		} else {
			g.setColor(Color.WHITE);
		}
		g.drawString("Selection", this.getWidth() / 4 + 200, this.getHeight() / 5);
		g.drawRect(this.getWidth() / 4 + 195, this.getHeight() / 5 - 15, 85, 20);
		g.setColor(Color.WHITE);
		g.drawString("Taille du tableau :", this.getWidth() / 4, (this.getHeight() * 2) / 5);
		g.drawRect(this.getWidth() / 4 + 150, (this.getHeight() * 2) / 5 - 15, 20, 20);
		g.drawRect(this.getWidth() / 4 + 250, (this.getHeight() * 2) / 5 - 15, 20, 20);
		g.drawString("-", this.getWidth() / 4 + 155, (this.getHeight() * 2) / 5);
		g.drawString("+", this.getWidth() / 4 + 255, (this.getHeight() * 2) / 5);
		g.drawString(""+this.tailleTableau, this.getWidth() / 4 + 175, (this.getHeight() * 2) / 5);
		g.drawString("Amplitude de valeurs :", this.getWidth() / 4, (this.getHeight() * 3) / 5);
		g.drawRect(this.getWidth() / 4 + 150, (this.getHeight() * 3) / 5 - 15, 20, 20);
		g.drawRect(this.getWidth() / 4 + 250, (this.getHeight() * 3) / 5 - 15, 20, 20);
		g.drawString("-", this.getWidth() / 4 + 155, (this.getHeight() * 3) / 5);
		g.drawString("+", this.getWidth() / 4 + 255, (this.getHeight() * 3) / 5);
		g.drawString(""+this.amplitude, this.getWidth() / 4 + 175, (this.getHeight() * 3) / 5);
		g.fillRect(this.getWidth()/4, (this.getHeight() * 7) / 10, this.getHeight() / 2, this.getWidth() / 5);
		g.setColor(Color.BLACK);
		g.drawString("Lancer l'animation", this.getWidth() / 3, (this.getHeight() * 4) / 5);
	}
	class Panneau extends JPanel {
		@Override public void paintComponent(Graphics g) {
			if(Vue.this.donnees.isEmpty()) {
				Vue.this.afficherMenu(g);
			} else {
				Vue.this.peindre(g);
			}
		}
	}
	@Override public void mouseClicked(MouseEvent e) {
		if(this.algo != null && this.donnees != null && !this.donnees.isEmpty()) {
			if(e.getY() >= this.getHeight() - 115 && e.getY() <= this.getHeight() - 95) {
				if(e.getX() >= 195 && e.getX() <= 215) {
					if(this.algo.getState() == Thread.State.TERMINATED) {
						JOptionPane jop1 = new JOptionPane();
						jop1.showMessageDialog(null,
							"L'algorithme a déjà terminé",
							"Algorithme terminé",
							JOptionPane.INFORMATION_MESSAGE);
					} else {
						for(int i = 0; i < (this.ctrl ? 10 : 1); i++) {
							this.algo.stepUp();
						}
					}
				} else if(e.getX() >= 305 && e.getX() <= 325) {
					if(this.algo.fps > (this.ctrl ? 10 : 1)) {
						this.algo.fps -= this.ctrl ? 10 : 1;
					}
				} else if(e.getX() >= 355 && e.getX() <= 375) {
					this.algo.fps += this.ctrl ? 10 : 1;
				}
			} else if(e.getY() >= this.getHeight() - 64 && e.getY() <= this.getHeight() - 45) {
				if(e.getX() >= 25 && e.getX() <= 125) {
					this.algo.tourne = !this.algo.tourne;
				} else if(e.getX() >= 270 && e.getX() <= 370) {
					this.algo = null;
					this.donnees = new Univers();
				}
			}
		} else {
			if(e.getY() >= this.getHeight() / 5 - 15 && e.getY() <= this.getHeight() / 5 + 5) {
				if(e.getX() >= this.getWidth() / 4 + 95 && e.getX() <= this.getWidth() / 4 + 180) {
					this.typeAlgo = Algo.Type.BULLE;
				} else if(e.getX() >= this.getWidth() / 4 + 195 && e.getX() <= this.getWidth() / 4 + 280) {
					this.typeAlgo = Algo.Type.SELECTION;
				}
			} else if (e.getY() >= (this.getHeight() * 2) / 5 - 15 && e.getY() <=  (this.getHeight() * 2) / 5 + 5) {
				if(e.getX() >= this.getWidth() / 4 + 150 && e.getX() <= this.getWidth() / 4 + 170) {
					for(int i = 0; i < (this.ctrl ? 10 : 1) && this.tailleTableau > 0; i++) {
						this.tailleTableau --;
					}
				} else if(e.getX() >= this.getWidth() / 4 + 250 && e.getX() <= this.getWidth() / 4 + 270) {
					this.tailleTableau += this.ctrl ? 10 : 1;
				}
			} else if (e.getY() >= (this.getHeight() * 3) / 5 - 15 && e.getY() <=  (this.getHeight() * 3) / 5 + 5) {
				if(e.getX() >= this.getWidth() / 4 + 150 && e.getX() <= this.getWidth() / 4 + 170) {
					for(int i = 0; i < (this.ctrl ? 10 : 1) && this.amplitude > 0; i++) {
						this.amplitude --;
					}
				} else if(e.getX() >= this.getWidth() / 4 + 250 && e.getX() <= this.getWidth() / 4 + 270) {
					this.amplitude += this.ctrl ? 10 : 1;
				}
			} else if(e.getX() >= this.getWidth() / 4 && e.getX() <= (3 * this.getWidth()) / 4
				&& e.getY() >= (this.getHeight() * 7) / 10 && e.getY() <= (this.getHeight() * 9) / 10) {
				Lanceur.lancer(this.typeAlgo, this.tailleTableau, this.amplitude);
			}
		}
	}
	@Override public void mousePressed(MouseEvent e) {}
	@Override public void mouseReleased(MouseEvent e) {}
	@Override public void mouseEntered(MouseEvent e) {}
	@Override public void mouseExited(MouseEvent e) {}
	@Override public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_CONTROL || e.getKeyCode() == KeyEvent.VK_COMPOSE) {
			this.ctrl = true;
		} else {
			System.out.println(e.getKeyCode());
		}
	}
	@Override public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_CONTROL || e.getKeyCode() == KeyEvent.VK_COMPOSE) {
			this.ctrl = false;
		}
	}
	@Override public void keyTyped(KeyEvent e) {}
}
