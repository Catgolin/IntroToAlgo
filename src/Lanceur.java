package patatedouce.algo;

public class Lanceur {
	private static Vue vue;
	public static void main(String[] args) {
		Lanceur.vue = new Vue();
		Thread affichage = new Thread(Lanceur.vue);
		affichage.start();
	}
	public static void lancer(Algo.Type typeAlgo, int tailleTableau, int amplitude) {
		Univers univers = new Univers(tailleTableau, amplitude);
		Algo algo;
		switch(typeAlgo) {
			case SELECTION:
				algo = new TriSelection(univers);
				break;
			case BULLE:
			default:
				algo = new TriBulle(univers);
				break;
		}
		algo.start();
		Lanceur.vue.setAlgo(algo);
		Lanceur.vue.setDonnees(univers);
	}
}
