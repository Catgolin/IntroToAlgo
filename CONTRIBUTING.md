**FR**

Pour rajouter un type de tri :
* Implementer l'interface Sort dans le dossier src/sorting
* Ne pas oublier la ligne "v.repaint();" pour l'update de la vue
* Rajouter la ligne "set.add(new YourNewSort());" dans le constructeur de SortFactory

Il vous suffit alors de compiler de cette façon :
* javac -cp bin/ -d /bin src/sorting/YourNewSort.java src/sorting/SortFactory.java

<br><br>

**ENG** (well I tried)

Add a new sorting type :
* Implement the Sort interface in directory src/sorting
* Don't forget the line "v.repaint()" for the view update
* Add the line "set.add(new YourNewSort());" in the SortFactory constructor

You can then compile like this :
* javac -cp bin/ -d bin/ src/sorting/YouNewSort.java src/sorting/SortFactory.java